import { BitcointradingPage } from './app.po';

describe('bitcointrading App', function() {
  let page: BitcointradingPage;

  beforeEach(() => {
    page = new BitcointradingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
